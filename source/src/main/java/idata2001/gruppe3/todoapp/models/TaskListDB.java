package idata2001.gruppe3.todoapp.models;

import idata2001.gruppe3.todoapp.data.Task;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TaskListDB implements TaskList {
    private final EntityManagerFactory emf;
    private final EntityManager em;

    public TaskListDB() {
        this.emf = Persistence.createEntityManagerFactory("todo-pu");
        this.em = emf.createEntityManager();
    }

    @Override
    public void addTask(Task task) {
        this.em.getTransaction().begin();
        this.em.persist(task);
        this.em.getTransaction().commit();
    }

    @Override
    public void removeTask(Task task) {
        this.em.getTransaction().begin();
        this.em.remove(task);
        this.em.getTransaction().commit();
    }

    @Override
    public List<Task> getOverDeadlineTasks() {
        ArrayList<Task> overDeadlineTasks = new ArrayList<>();

        for (Task task : this) {
            long hoursDifference = ChronoUnit.HOURS.between(task.getDateCreated(), task.getDeadline());
            if(hoursDifference <= 0) {
                overDeadlineTasks.add(task);
            }
        }

        return overDeadlineTasks;
    }

    @Override
    public List<Task> getTodaysTasks() {
        ArrayList<Task> todaysTasks = new ArrayList<>();

        for (Task task : this) {
            long hoursDifference = ChronoUnit.HOURS.between(task.getDateCreated(), task.getDeadline());

            if(hoursDifference < 24 && hoursDifference > 0) {
                todaysTasks.add(task);
            }
        }

        return todaysTasks;
    }

    @Override
    public List<Task> getWeeksTasks() {
        ArrayList<Task> weeksTasks = new ArrayList<>();

        for (Task task : this) {
            long daysDifference = ChronoUnit.DAYS.between(task.getDateCreated(), task.getDeadline());
            long secondsDifference = ChronoUnit.SECONDS.between(task.getDateCreated(), task.getDeadline());
            if (daysDifference < 7 && secondsDifference > 0) {
                weeksTasks.add(task);
            }
        }

        return weeksTasks;
    }

    @Override
    public List<Task> getAllTasks() {
        ArrayList<Task> tasks = new ArrayList<>();

        for(Task task : this) {
            tasks.add(task);
        }

        return tasks;
    }

    @Override
    public Iterator<Task> iterator() {
        List<Task> taskList;
        String jpql = "SELECT t FROM Task t";
        TypedQuery<Task> query = this.em.createQuery(jpql, Task.class);
        taskList = query.getResultList();
        return taskList.iterator();
    }
}
