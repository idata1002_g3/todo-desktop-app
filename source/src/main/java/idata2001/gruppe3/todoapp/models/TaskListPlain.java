package idata2001.gruppe3.todoapp.models;

import idata2001.gruppe3.todoapp.data.Task;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;

public class TaskListPlain implements TaskList {
    private ArrayList<Task> tasks;

    public TaskListPlain() {
        this.tasks = new ArrayList<>();
    }

    @Override
    public void addTask(Task task) {
        this.tasks.add(task);
    }

    @Override
    public void removeTask(Task task) {
        this.tasks.remove(task);
    }

    @Override
    public ArrayList<Task> getOverDeadlineTasks() {
        ArrayList<Task> overDeadlineTasks = new ArrayList<>();

        for (Task task : tasks) {
            long hoursDifference = ChronoUnit.HOURS.between(task.getDateCreated(), task.getDeadline());
            if(hoursDifference <= 0) {
                overDeadlineTasks.add(task);
            }
        }

        return overDeadlineTasks;
    }

    @Override
    public ArrayList<Task> getTodaysTasks() {
        ArrayList<Task> todaysTasks = new ArrayList<>();

        for (Task task : tasks) {
            long hoursDifference = ChronoUnit.HOURS.between(task.getDateCreated(), task.getDeadline());

            if(hoursDifference < 24 && hoursDifference > 0) {
                todaysTasks.add(task);
            }
        }

        return todaysTasks;
    }

    @Override
    public ArrayList<Task> getWeeksTasks() {
        ArrayList<Task> weeksTasks = new ArrayList<>();

        for (Task task : tasks) {
            long daysDifference = ChronoUnit.DAYS.between(task.getDateCreated(), task.getDeadline());
            long secondsDifference = ChronoUnit.SECONDS.between(task.getDateCreated(), task.getDeadline());
            if (daysDifference < 7 && secondsDifference > 0) {
                weeksTasks.add(task);
            }
        }

        return weeksTasks;
    }

    @Override
    public ArrayList<Task> getAllTasks() {
        return this.tasks;
    }

    @Override
    public Iterator<Task> iterator() {
        return this.tasks.iterator();
    }
}
