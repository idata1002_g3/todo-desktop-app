package idata2001.gruppe3.todoapp.models;

import idata2001.gruppe3.todoapp.data.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represents the interface for the task list class
 * Supports CRUD-operations
 */
public interface TaskList extends Iterable<Task> {
    /**
     * Adds a task to the task list
     * @param task task to add
     */
    public void addTask(Task task);

    /**
     * Removes a task from the list
     * @param task task to remove
     */
    public void removeTask(Task task);

    /**
     * Gets the tasks with deadline in the next 7 days
     * @return list of task with deadline within next 7 days
     */
    public List<Task> getWeeksTasks();

    /**
     * Gets the tasks with deadline on the same day
     * @return list of task with deadline within the same day
     */
    public List<Task> getTodaysTasks();

    /**
     * Gets the tasks that are over the deadline
     * @return list of task over the deadline
     */
    public List<Task> getOverDeadlineTasks();

    /**
     * Gets all the current task from the task list
     * @return all current tasks
     */
    public List<Task> getAllTasks();

    @Override
    Iterator<Task> iterator();
}
