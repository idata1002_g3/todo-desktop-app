package idata2001.gruppe3.todoapp.ui.controllers;


import idata2001.gruppe3.todoapp.models.TaskList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import idata2001.gruppe3.todoapp.data.Task;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * This class controls the Calendar page View
 */
public class CalendarController {
    @FXML
    private Label localMonthYear; //Label that tells which month and year the calender is currently showing

    @FXML
    private GridPane calenderPane; //The gridpane that makes up the calender

    public LocalDateTime localTimeControl; //A variable that holds the current month, at default this hold the month that the users computer is currently set to

    public Button buttonNextMonth;

    public Button buttonPrevMonth;

    private TaskList taskList;

    /**
     * Gives the column of the gridpane with includes the days of the week
     * @param localMonthTime the current month
     * @return the variable i that lets loadMonth set the correct dates for that specific day in the month given
     */
    private int getColumn(LocalDateTime localMonthTime){
        int i = 0;
        while(localMonthTime.getDayOfWeek() != DayOfWeek.SUNDAY){
            i++;
            localMonthTime = localMonthTime.plusDays(1);
        }

        return i;
    }

    /**
     * Sets the task list
     * @param taskList task list to set
     */
    public void setTaskList(TaskList taskList) {
        this.taskList = taskList;
    }

    /**
     * Gets the amount of tasks for specified day
     * @param date the date to check for
     * @return tasks in the specified day
     */
    private int getTasksThisDay(LocalDateTime date) {
        int tasksThisDay = 0;
        for(Task task : this.taskList) {
            long daysDifference = ChronoUnit.DAYS.between(date, task.getDeadline());
            if(Math.abs(daysDifference) <= 1 && date.getDayOfMonth() == task.getDeadline().getDayOfMonth()) {
                tasksThisDay++;
            }
        }
        return tasksThisDay;
    }

    /**
     * Loads the days in the selected month in a grid
     * @param localMonthTime the current month shown
     */
    public void loadMonth(LocalDateTime localMonthTime){
        this.localTimeControl = localMonthTime;

        if (this.calenderPane.getChildren().size() > 0){
            this.calenderPane.getChildren().clear();
        }

        setGridPaneFirstRow();
        localMonthYear.setText(localMonthTime.getMonth() + " " + localMonthTime.getYear());
        localMonthYear.setStyle("-fx-font-weight: bold; -fx-font-size: 16px");

        LocalDateTime timeIterator = localMonthTime.minusDays(localMonthTime.getDayOfMonth() - 1);
        timeIterator.format(DateTimeFormatter.ISO_DATE);

        int control1 = getColumn(timeIterator);
        int control2 = 0;
        int i = 0;

        while(timeIterator.getMonth() == localMonthTime.getMonth()){
            if(i == 0 || i == 1 && control2 <= control1){
                i = 1;
                control2++;
            } else {
                i = ((control2 - (control1 + 1)) / 7) + 2;
                control2++;
            }

            VBox container = new VBox();
            container.setAlignment(Pos.TOP_RIGHT);
            Label tempLabel = new Label(Integer.toString(timeIterator.getDayOfMonth()));
            tempLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 14px");

            container.getChildren().add(tempLabel);

            int tasksThisDay = this.getTasksThisDay(timeIterator);

            if(tasksThisDay > 0) {
                Label tasksThisDayLabel = new Label(tasksThisDay + " tasks");
                tasksThisDayLabel.setStyle("-fx-font-size: 14px");
                container.getChildren().add(tasksThisDayLabel);
            }

            this.calenderPane.add(container, (timeIterator.getDayOfWeek().getValue() - 1), i);

            timeIterator = timeIterator.plusDays(1);
        }
    }

    /**
     * Sets the values of the first row to the days of a week
     */
    private void setGridPaneFirstRow(){
        String[] string = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        for (int i = 0; i < string.length; i++){
            Label dayLabel = new Label(string[i]);
            dayLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 16px");
            GridPane.setHalignment(dayLabel, HPos.CENTER);
            calenderPane.add(dayLabel, i, 0);
        }
    }

    /**
     * Changes the current month to the next and reloads the month in the calender
     * @param actionEvent the nextMonth/right arrow button gets pressed
     */
    public void handleNextMonth(ActionEvent actionEvent){
        localTimeControl = localTimeControl.plusMonths(1);
        loadMonth(localTimeControl);
    }

    /**
     * Changes the current month to the previous and reloads the month in the calender
     * @param actionEvent the prevMonth/left arrow button gets pressed
     */
    public void handlePrevMonth(ActionEvent actionEvent){
        localTimeControl = localTimeControl.minusMonths(1);
        loadMonth(localTimeControl);
    }
}
