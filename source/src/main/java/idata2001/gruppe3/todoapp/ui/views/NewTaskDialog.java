package idata2001.gruppe3.todoapp.ui.views;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import tornadofx.control.DateTimePicker;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is a dialog that used used for creation of a Task
 */
public class NewTaskDialog {
    public static HashMap<String, String> data = new HashMap<>();

    private static boolean validInput(String input) {
        return input != null && !input.isEmpty();
    }

    /**
     * Displays the dialog and returns the data input in the dialog
     * It also checks for wrong input and notifies the user
     * @return map of input data
     */
    public static HashMap<String, String> display() {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);

        VBox mainContainer = new VBox();

        Label titleInputLabel = new Label("Title");
        TextField titleInput = new TextField();

        Label descriptionInputLabel = new Label("Description");
        TextArea descriptionInput = new TextArea();

        Label datePickerLabel = new Label("Date");
        DateTimePicker datePicker = new DateTimePicker();

        VBox submitButtonContainer = new VBox();
        Button submitButton = new Button("Submit");
        submitButton.setMinWidth(75);
        submitButtonContainer.getChildren().add(submitButton);
        submitButtonContainer.setAlignment(Pos.BOTTOM_RIGHT);

        submitButton.setOnAction(e -> {
            String title = titleInput.getText();
            String description = descriptionInput.getText();
            String dateTime = datePicker.getEditor().getText();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime date = LocalDateTime.parse(dateTime, formatter);

            if(validInput(title) && validInput(description) && validInput(date.toString())) {
                data.put("title", titleInput.getText());
                data.put("description", descriptionInput.getText());
                data.put("date", date.toString());

                stage.close();
            }

            else {
                Alert alert = new Alert(Alert.AlertType.WARNING,"You cannot leave any fields empty. Please try again!", ButtonType.CLOSE);
                alert.showAndWait();
            }
        });

        mainContainer.setPadding(new Insets(10, 10, 10, 10));
        mainContainer.setSpacing(10);

        mainContainer.getChildren().addAll(
                titleInputLabel,
                titleInput,
                descriptionInputLabel,
                descriptionInput,
                datePickerLabel,
                datePicker,
                submitButtonContainer
        );

        Scene scene = new Scene(mainContainer, 400, 300);
        stage.setResizable(false);
        stage.setTitle("Add new task");
        stage.setScene(scene);
        stage.showAndWait();

        return data;
    }
}
