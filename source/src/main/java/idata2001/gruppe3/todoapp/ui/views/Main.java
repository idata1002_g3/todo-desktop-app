package idata2001.gruppe3.todoapp.ui.views;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The main window of the application
 * It loads in the main fxml file to display the interface
 */
public class Main extends Application {
    /**
     * This method gets executed at the start of the application.
     * It loads in the FXML file to display the interface
     * @param stage root stage
     * @throws Exception exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/main.fxml"));

        stage.setTitle("TODO List App");
        stage.setScene(new Scene(root, 800, 600));
        stage.show();
    }
}