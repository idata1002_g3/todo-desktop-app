package idata2001.gruppe3.todoapp.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;

/**
 * This class controls the ActiveTasks page View
 */
public class ActiveTasksController {
    @FXML
    private VBox tasksOverDeadlineContainer;
    @FXML
    private VBox tasksTodayContainer;
    @FXML
    private VBox tasksThisWeekContainer;
    @FXML
    private VBox allTasksContainer;

    @FXML
    private TitledPane tasksOverDeadlinePane;
    @FXML
    private TitledPane tasksTodayPane;
    @FXML
    private TitledPane tasksThisWeekPane;
    @FXML
    private TitledPane allTasksPane;

    /**
     * Clears all the tasks in the over deadline container
     */
    public void clearTasksOverDeadlineContainer() {
        this.tasksOverDeadlineContainer.getChildren().clear();
    }

    /**
     * Clears all the tasks in the tasks today container
     */
    public void clearTasksTodayContainer() {
        this.tasksTodayContainer.getChildren().clear();
    }

    /**
     * Clears all the tasks in the this week container
     */
    public void clearTasksThisWeekContainer() {
        this.tasksThisWeekContainer.getChildren().clear();
    }

    /**
     * Clears all the tasks in the all tasks container
     */
    public void clearAllTasksContainer() {
        this.allTasksContainer.getChildren().clear();
    }

    /**
     * Adds a container to the over deadline container
     * @param container container to add
     */
    public void addToTasksOverDeadlineContainer(Node container) {
        this.tasksOverDeadlineContainer.getChildren().addAll(container);
    }

    /**
     * Displays the count of the tasks in the over deadline pane
     * @param count amount tasks
     */
    public void setTasksOverDeadlineCount(int count) {
        this.tasksOverDeadlinePane.setText("You have (" + count + ") tasks over deadline");
    }

    /**
     * Adds a container to the tasks today container
     * @param container container to add
     */
    public void addToTasksTodayContainer(Node container, int count) {
        this.tasksTodayContainer.getChildren().addAll(container);
    }

    /**
     * Displays the count of the tasks in the tasks today pane
     * @param count amount tasks
     */
    public void setTasksTodayCount(int count) {
        this.tasksTodayPane.setText("Today (" + count + ")");
    }

    /**
     * Adds a container to the this week container
     * @param container container to add
     */
    public void addToTasksThisWeekContainer(Node container, int count) {
        this.tasksThisWeekContainer.getChildren().addAll(container);
    }

    /**
     * Displays the count of the tasks in the next 7 days pane
     * @param count amount tasks
     */
    public void setTasksThisWeekCount(int count) {
        this.tasksThisWeekPane.setText("Next 7 days (" + count + ")");
    }

    /**
     * Adds a container to the all tasks container
     * @param container container to add
     */
    public void addToAllTasksContainer(Node container, int count) {
        this.allTasksContainer.getChildren().addAll(container);
    }

    /**
     * Displays the count of the tasks in the all tasks pane
     * @param count amount tasks
     */
    public void setAllTasksCount(int count) {
        this.allTasksPane.setText("All tasks (" + count + ")");

    }
}
