package idata2001.gruppe3.todoapp.ui.views.pages;

import idata2001.gruppe3.todoapp.ui.controllers.ActiveTasksController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

/**
 * This class is a singleton used for a page
 * It creates an instance of itself containing the container, as well as the controller
 * Used for loading in the FXML of the View
 */
public class ActiveTasks {
    private Parent container;
    private ActiveTasksController controller;

    private static ActiveTasks instance;

    /**
     * Creates the instance of the class, loading in the FXML
     * It also sets the container and controller
     */
    private ActiveTasks() {
        this.loadContainer();
    }

    /**
     * Loads in the container and controller from FXML
     */
    private void loadContainer() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            this.container = fxmlLoader.load(getClass().getClassLoader().getResource("fxml/pages/activeTasks.fxml").openStream());
            this.controller = fxmlLoader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the instance of this class,
     * if the instance doesn't exist it gets created
     * @return the instance
     */
    public static ActiveTasks getInstance() {
        if(instance == null) {
            instance = new ActiveTasks();
        }
        return instance;
    }

    /**
     * Returns the controller of this class
     * @return the controller
     */
    public ActiveTasksController getController() {
        return this.controller;
    }

    /**
     * Returns the container of this class
     * @return the container
     */
    public Parent getContainer(){
        return this.container;
    }
}
