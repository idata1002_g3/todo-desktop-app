package idata2001.gruppe3.todoapp.ui.controllers;

import idata2001.gruppe3.todoapp.models.TaskList;
import idata2001.gruppe3.todoapp.data.Task;
import idata2001.gruppe3.todoapp.models.TaskListDB;
import idata2001.gruppe3.todoapp.models.TaskListPlain;
import idata2001.gruppe3.todoapp.ui.views.NewTaskDialog;
import idata2001.gruppe3.todoapp.ui.views.pages.ActiveTasks;
import idata2001.gruppe3.todoapp.ui.views.pages.AllTasks;
import idata2001.gruppe3.todoapp.ui.views.pages.Calendar;
import javafx.fxml.FXML;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

/**
 * This class controls the Main page View
 */
public class MainController {
    @FXML
    private VBox pageContainer;
    @FXML
    private Button activeTasksButton;

    private int currentPageIndex; // 0 = Active tasks | 1 = Calendar | 2 = All tasks

    private final TaskList taskList;

    private Image deleteIcon;
    private Image completeIcon;

    /**
     * Creates the instance of the class
     */
    public MainController() {
        // Prepare some sample data
        this.taskList = new TaskListDB();
    }

    /**
     * Initialize method called after all FXML fields are populated
     * This method is called after the class constructor
     * Displays the page at the start of the application
     */
    @FXML
    public void initialize() {
        this.deleteIcon = new Image(getClass().getResourceAsStream("/icons/delete.png"), 20, 20, false, false);
        this.completeIcon = new Image(getClass().getResourceAsStream("/icons/check.png"), 20, 20, false, false);
        this.switchToActiveTasksPage();
    }

    /**
     * Clears the page container
     */
    public void clearPageContainer() {
        this.pageContainer.getChildren().clear();
    }

    /**
     * Reloads the current page based on which one is currently open
     */
    public void refreshCurrentPage() {
        switch(this.currentPageIndex) {
            case 0:
                this.switchToActiveTasksPage();
                break;
            case 1:
                this.switchToCalendarPage();
                break;
            case 2:
                this.switchToAllTasksPage();
                break;
            default:
                break;
        }
    }

    /**
     * Clears and displays the new container to the page container
     * @param container page container to switch to
     */
    public void switchToPage(Parent container) {
        this.clearPageContainer();
        this.pageContainer.getChildren().addAll(container);
    }

    /**
     * Switches current page to active tasks page
     */
    public void switchToActiveTasksPage() {
        this.currentPageIndex = 0;
        this.switchToPage(ActiveTasks.getInstance().getContainer());
        this.displayTasksOverDeadline();
        this.displayTasksToday();
        this.displayTasksNextWeek();
        this.displayAllTasks();
    }

    /**
     * Switches current page to calendar page
     */
    public void switchToCalendarPage() {
        this.currentPageIndex = 1;
        this.switchToPage(Calendar.getInstance().getContainer());
        this.displayCurrentMonth();
    }

    /**
     * Loads in the current month for calendar page
     */
    private void displayCurrentMonth() {
        Calendar.getInstance().getController().setTaskList(this.taskList);
        Calendar.getInstance().getController().loadMonth(LocalDateTime.now());
    }

    /**
     * Switches current page to all tasks page
     */
    public void switchToAllTasksPage() {
        this.currentPageIndex = 2;
        this.switchToPage(AllTasks.getInstance().getContainer());
        this.displayTasksAllTasksPage();
    }

    @FXML
    public void onNewTaskButtonPressed(ActionEvent actionEvent) {
        HashMap<String, String> data = NewTaskDialog.display();
        if(!data.isEmpty()) {
            LocalDateTime dateTime = LocalDateTime.parse(data.get("date"));
            this.taskList.addTask(new Task(dateTime, data.get("title"), data.get("description")));
            this.refreshCurrentPage();
        }
    }

    @FXML
    public void onActiveTasksButtonPressed(ActionEvent actionEvent) {
        this.clearPageContainer();
        this.switchToActiveTasksPage();
    }

    @FXML
    public void onCalendarButtonPressed(ActionEvent actionEvent) {
        this.clearPageContainer();
        this.switchToCalendarPage();
    }

    @FXML
    public void onAllTasksButtonPressed(ActionEvent actionEvent) {
        this.clearPageContainer();
        this.switchToAllTasksPage();
    }

    @FXML
    public void onDeleteTaskButtonClicked(Task task) {
        taskList.removeTask(task);
        this.refreshCurrentPage();
    }

    @FXML
    public void onSetTaskCompletedButtonClicked(Task task) {
        task.setCompleted(true);
        this.refreshCurrentPage();
    }

    /**
     * Creates and returns a javaFX container based on given Task object
     * @param task task to get the data from
     * @return the Node container of the task
     */
    public Node getTaskContainer(Task task) {
        VBox mainContainer = new VBox();
        BorderPane topContainer = new BorderPane();

        Label titleLabel = new Label(task.getTitle());
        Label descriptionTextArea = new Label(task.getDescription());
        Label dateLabel = new Label("Deadline: " + task.getDeadline().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));

        ImageView deleteIconView = new ImageView(this.deleteIcon);
        Button deleteButton = new Button("", deleteIconView);

        ImageView completeIconView = new ImageView(this.completeIcon);
        Button setCompletedButton = new Button("", completeIconView);

        titleLabel.setStyle("-fx-font-size: 16px");
        descriptionTextArea.setStyle("-fx-font-size: 14px");
        dateLabel.setStyle("-fx-font-size: 14px");
        deleteButton.setStyle("-fx-background-color: transparent");
        setCompletedButton.setStyle("-fx-background-color: transparent");

        mainContainer.setStyle("-fx-border-color: black; -fx-border-width: 0.5");
        mainContainer.setPadding(new Insets(10));
        mainContainer.setSpacing(20);

        deleteButton.setOnAction(event -> onDeleteTaskButtonClicked(task));
        setCompletedButton.setOnAction(event -> onSetTaskCompletedButtonClicked(task));

        topContainer.setLeft(setCompletedButton);
        topContainer.setCenter(titleLabel);
        topContainer.setRight(deleteButton);

        mainContainer.getChildren().addAll(
                topContainer,
                descriptionTextArea,
                dateLabel
        );

        return mainContainer;
    }

    /**
     * Displays the all tasks page
     */
    public void displayTasksAllTasksPage() {
        AllTasks.getInstance().getController().clearTaskContainer();
        for (Task task : this.taskList) {
            AllTasks.getInstance().getController().addTaskVBoxContainer(this.getTaskContainer(task));
        }
    }

    /**
     * Displays the tasks over deadline for the active tasks page
     */
    public void displayTasksOverDeadline() {
        ActiveTasks.getInstance().getController().clearTasksOverDeadlineContainer();
        int added = 0;
        for(Task task : this.taskList.getOverDeadlineTasks()) {
            if(!task.isCompleted()) {
                added++;
                ActiveTasks.getInstance().getController().addToTasksOverDeadlineContainer(this.getTaskContainer(task));
            }
        }
        ActiveTasks.getInstance().getController().setTasksOverDeadlineCount(added);
    }

    /**
     * Displays the tasks today for the active tasks page
     */
    public void displayTasksToday() {
        ActiveTasks.getInstance().getController().clearTasksTodayContainer();
        int added = 0;
        for(Task task : this.taskList.getTodaysTasks()) {
            if(!task.isCompleted()) {
                added++;
                ActiveTasks.getInstance().getController().addToTasksTodayContainer(this.getTaskContainer(task), added);
            }
        }
        ActiveTasks.getInstance().getController().setTasksTodayCount(added);
    }

    /**
     * Displays the tasks next 7 days for the active tasks page
     */
    public void displayTasksNextWeek() {
        ActiveTasks.getInstance().getController().clearTasksThisWeekContainer();
        int added = 0;
        for(Task task : this.taskList.getWeeksTasks()) {
            if(!task.isCompleted()) {
                added++;
                ActiveTasks.getInstance().getController().addToTasksThisWeekContainer(this.getTaskContainer(task), added);
            }
        }
        ActiveTasks.getInstance().getController().setTasksThisWeekCount(added);
    }

    /**
     * Displays all the tasks for the active tasks page
     */
    public void displayAllTasks() {
        ActiveTasks.getInstance().getController().clearAllTasksContainer();
        int added = 0;
        for (Task task : this.taskList) {
            if (!task.isCompleted()) {
                added++;
                ActiveTasks.getInstance().getController().addToAllTasksContainer(this.getTaskContainer(task), added);
            }
        }
        ActiveTasks.getInstance().getController().setAllTasksCount(added);
    }
}
