package idata2001.gruppe3.todoapp.ui.controllers;

import javafx.scene.Node;
import javafx.scene.layout.VBox;

/**
 * This class controls the AllTasks page View
 */
public class AllTasksController {
    public VBox pageContainer;

    /**
     * Clears the task container
     */
    public void clearTaskContainer() {
        pageContainer.getChildren().clear();
    }

    /**
     * Adds the task container to the page container in order to display it
     * @param taskVBoxContainer the container to add
     */
    public void addTaskVBoxContainer(Node taskVBoxContainer) {
        pageContainer.getChildren().addAll(taskVBoxContainer);
    }
}