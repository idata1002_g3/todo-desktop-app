package idata2001.gruppe3.todoapp.data;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import java.time.LocalDateTime;

/**
 * Represents a task the be executed.
 * The task has description, deadline and a title as well as some other functions
 */
@Entity
public class Task {
    @Id
    @GeneratedValue
    private Integer id;

    private final LocalDateTime dateCreated;
    private String description;
    private LocalDateTime deadline;
    private String title;
    private boolean completed;

    /**
     * Constructor of the class, creates new instance of a task
     * @param deadline the deadline date
     * @param title title of the task
     * @param description description of the task
     */
    public Task(LocalDateTime deadline, String title, String description) {
        this.deadline = deadline;
        this.title = title;
        this.description = description;
        this.completed = false;
        this.dateCreated = LocalDateTime.now();
    }

    /**
     * Constructor needed for persistence
     */
    public Task() {
        this(LocalDateTime.now(), "", "");
    }

    /**
     * Sets the title of the task
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets the task to comleted / not completed
     * @param state the state of completion
     */
    public void setCompleted(boolean state) {
        this.completed = state;
    }

    /**
     * Gets if the task is completed
     * @return true if completed, false otherwise
     */
    public boolean isCompleted() {
        return this.completed;
    }

    /**
     * Sets the description of the task
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the deadline of the task
     * @param date the date of the deadline
     */
    public void setDeadline(LocalDateTime date) {
        this.deadline = date;
    }

    /**
     * Gets the date the task was created in
     * @return the creation date
     */
    public LocalDateTime getDateCreated() {
        return this.dateCreated;
    }

    /**
     * Gets the deadline for the task
     * @return the deadline
     */
    public LocalDateTime getDeadline() {
        return this.deadline;
    }

    /**
     * Gets the title of the task
     * @return the title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Gets the description of the task
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }
}
